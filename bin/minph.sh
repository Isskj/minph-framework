#!/bin/bash

source "`dirname $0`/tc.common.sh"

_usage()
{
    tc.warning "$0 [install / uninstall ] [install directory]" && exit 1
    exit 1
}

#---------------------------------------------------
# parsing options
#---------------------------------------------------
cmd=""
directory=""
bin_dir=`dirname $0`
framework_dir=`find ${bin_dir}/../.. -name "minph-framework" -type d`

if [ -z "${framework_dir}" ]; then
    framework_dir="."
fi

if [ -n "$1" ]; then cmd=$1         ; fi
if [ -n "$2" ]; then directory=$2   ; fi

if ! [ -d "${directory}" ] || ! [ -d "${framework_dir}" ]; then
    _usage
fi

#---------------------------------------------------
# main
#---------------------------------------------------
case $cmd in
    install)
        tc.verbose_exec "cp -r ${framework_dir}/templates/* ${directory}"
        tc.verbose_exec "ln -s ${directory}/resource/dist ${directory}/public/"
        ;;
    uninstall)
        tc.verbose_exec "rm -rf ${directory}/app"
        tc.verbose_exec "rm -rf ${directory}/public"
        tc.verbose_exec "rm -rf ${directory}/resource"
        ;;
    *)
        _usage
        ;;
esac
