#!/bin/bash

source "`dirname $0`/tc.common.sh"

_usage()
{
    tc.warning "$0 [-d appdir] [-u user]" && exit 1
}

#---------------------------------------------------
# parsing options
#---------------------------------------------------
appdir=""
user=""
while getopts ":d:u:" opts; do
    case ${opts} in
        d)
            appdir=${OPTARG}
            ;;
        u)
            user=${OPTARG}
            ;;
        *)
            _usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${appdir}" ] || ! [ -d "${appdir}/migration" ] || [ -z "${user}" ]; then
    _usage
fi

#---------------------------------------------------
# main
#---------------------------------------------------
tc.verbose_exec "rm -rf dump.sql"
for file in `find ${appdir}/migration -name "*.sql" | sort`
do
    tc.verbose_exec "cat $file >> dump.sql"
done

tc.verbose_exec "mysql -u ${user} -p < dump.sql"
