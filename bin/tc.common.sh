#!/bin/bash

#------------------------------------------------
# color code
#------------------------------------------------
BLACK="\033[0;30m"
DARK_GRAY="\033[1;30m"
LIGHT_GRAY="\033[0;37m"
RED="\033[0;31m"
LIGHT_RED="\033[1;31m"
GREEN="\033[0;32m"
LIGHT_GREEN="\033[1;32m"
ORANGE="\033[0;33m"
YELLOW="\033[1;33m"
BLUE="\033[0;34m"
LIGHT_BLUE="\033[1;34m"
PURPLE="\033[0;35m"
LIGHT_PURPLE="\033[1;35m"
CYAN="\033[0;36m"
LIGHT_CYAN="\033[1;36m"
WHITE="\033[1;37m"
NC="\033[0m"

#------------------------------------------------
# functions
#------------------------------------------------
tc.success()
{
    echo -e "${GREEN}$1${NC}"
}
tc.warning()
{
    echo -e "${YELLOW}$1${NC}"
}
tc.info()
{
    echo -e "${CYAN}$1${NC}"
}
tc.error()
{
    echo -e "${RED}$1${NC}" && exit 1
}

tc.verbose_exec_failskip()
{
    echo "$1" && eval "$1"
}

tc.verbose_exec()
{
    echo "$1" && eval "$1"
    if [ "$?" != "0" ]; then
        tc.error "failed:[$1] aborted."
        exit 1
    fi
}

tc.check_command()
{
    which $1 2>/dev/null 1>&2
    if [ "$?" != "0" ]; then
        tc.error "[$1] is not installed."
        exit 1
    fi
}

