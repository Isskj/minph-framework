#!/bin/bash

source "`dirname $0`/tc.common.sh"

_usage()
{
    tc.warning "$0 [ directory ]" && exit 1
}

#---------------------------------------------------
# parsing options
#---------------------------------------------------
directory="$1"

if [ -n "$1" ]; then directory=$1 ; fi

if ! [ -d "${directory}" ]; then
    _usage
fi

#---------------------------------------------------
# main
#---------------------------------------------------
for file in `find "${directory}" -name "*.php"`
do
    tc.verbose_exec "php -l $file"
done
