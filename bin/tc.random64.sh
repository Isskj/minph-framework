#!/bin/bash

source "`dirname $0`/tc.common.sh"

_usage()
{
    tc.warning "$0 [-c count]" && exit 1
}

#---------------------------------------------------
# parsing options
#---------------------------------------------------
count=""
while getopts ":c:" opts; do
    case ${opts} in
        c)
            count=${OPTARG}
            ;;
        *)
            _usage
    esac
done
shift $((OPTIND-1))

if ! [ "${count}" -eq "${count}" ] 2>/dev/null ; then
    _usage
fi

#---------------------------------------------------
# main
#---------------------------------------------------
tc.verbose_exec "cat /dev/urandom | base64 | head -c ${count}"
