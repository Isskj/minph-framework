<?php

use PHPUnit\Framework\TestCase;
use Minph\MVC\App;
use Minph\Utility\Pool;
use Minph\Repository\DB;

class DBTest extends TestCase
{
    private $app;
    protected function setUp()
    {
        $this->app = App::boot(__DIR__.'/../app');
    }

    protected function tearDown()
    {
    }

    public function testAppBoot()
    {
        if (!Pool::exists('default')) {
            $db = new DB(
                $this->app->env('DATABASE_DSN'),
                $this->app->env('DATABASE_USERNAME'),
                $this->app->env('DATABASE_PASSWORD'));
            Pool::set('default', $db);
        }
    }
}
