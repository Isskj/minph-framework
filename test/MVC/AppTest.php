<?php

use PHPUnit\Framework\TestCase;
use Minph\MVC\App;

class AppTest extends TestCase
{
    protected function setUp()
    {
        App::boot(__DIR__.'/../app');
    }

    protected function tearDown()
    {
    }

    public function testAppBoot()
    {
        $_SERVER['REQUEST_URI'] = 'http://localhost:12345/jp/test';
        $app = App::instance();
        $ret = $app->debugger()
                   ->template($app->make('/template/TemplateSmarty'))
                   ->run();
        $this->assertTrue($ret);
    }
}
