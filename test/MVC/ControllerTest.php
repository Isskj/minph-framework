<?php

use PHPUnit\Framework\TestCase;
use Minph\MVC\App;
use Minph\MVC\Controller;

class ControllerTest extends TestCase
{
    protected function setUp()
    {
        App::boot(__DIR__.'/../app');
    }

    protected function tearDown()
    {
    }

    public function testController()
    {
        $controller = new Controller(__DIR__ .'/../app');
        $uri = 'http://localhost:12345/jp/test';
        $ret = $controller->route($uri);
        $this->assertTrue($ret);

        $uri = 'http://localhost:12345/jp/test/sample/11111/';
        $ret = $controller->route($uri);
        $this->assertTrue($ret);

        $uri = 'http://localhost:12345/api/test';
        $ret = $controller->route($uri);
        $this->assertTrue($ret);

        $uri = 'http://localhost:12345/web/test';
        $ret = $controller->route($uri);
        $this->assertTrue($ret);
    }
}


