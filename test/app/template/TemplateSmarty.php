<?php

use Minph\View\Template;


class TemplateSmarty implements Template
{
    private $engine;

    public function __construct()
    {
        $this->engine = new Smarty();
        $this->engine->setTemplateDir(MINPH_APP_HOME . '/view');
        $this->engine->setCompileDir(MINPH_APP_HOME . '/storage/template/smarty/templates_c');
        $this->engine->setCacheDir(MINPH_APP_HOME . '/storage/template/smarty/cache');
        $this->engine->error_reporting = 0;
        if (getenv('DEBUG') === 'true') {
            $this->engine->debugging = true;
        }
    }

    public function html($file, $model = null)
    {
        if (!empty($model)) {
            $this->engine->assign($model);
        }
        $this->engine->display($file);
    }
}
