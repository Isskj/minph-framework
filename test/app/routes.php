<?php

return [
    '/jp/test' => 'TestController@index',
    '/jp/test/sample/[0-9]+/' => 'TestController@number',
    '/api/test' => 'api/APIController@index',
    '/web/test' => 'web/WebController@index'
];
