<?php

use PHPUnit\Framework\TestCase;
use Minph\Crypto\EncoderAES256;


final class CryptoTest extends TestCase
{
    const AES256_CBC_KEY = '01234567890123456789012345678901';
    const BYTES_LEN = 36;
    private $encoder;

    protected function setUp()
    {
        $this->encoder = new EncoderAES256(self::AES256_CBC_KEY);
    }

    protected function tearDown()
    {
    }

    public function testEncrypt()
    {
        $origin = random_bytes(self::BYTES_LEN);
        echo "\norigin:$origin\n";
        $enc = $this->encoder->encrypt($origin);
        echo "enc:$enc\n";
        $dec = $this->encoder->decrypt($enc);
        echo "dec:$dec\n";
        $this->assertSame($origin, $dec);
    }
}


