<?php

use PHPUnit\Framework\TestCase;
use Minph\Http\Session;
use Minph\App;

if ( !isset( $_SESSION ) ) $_SESSION = array(  );


class SessionTest extends TestCase
{
    private $routes = [
        '/jp/ja/product/item' => 'api/item@method',
        '/jp/ja/product/item/search' => 'api/itemSearch@method',
        '/jp/ja/product/price' => 'price',
        '/jp/ja/product/[0-9]+/' => 'api/priceNum@index',
        '/jp/ja/product/price/search' => 'priceSearch',
    ];

    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testRoutes()
    {
        $this->assertSame(1,1);
    }
}


