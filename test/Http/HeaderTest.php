<?php

use PHPUnit\Framework\TestCase;
use Minph\Http\Header;

$_SERVER = [
    'HTTP_CONTENT_TYPE' => 'application/json',
    'HTTP_AUTHORIZATION' => 'Bearer testtesttest',
];


class SessionTest extends TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testHeader()
    {
        $obj = new Header();
        $headers = $obj->get();
        $this->assertSame('Bearer testtesttest', $headers['Authorization']);
        $this->assertSame('testtesttest', $obj->authBearer());
        $this->assertSame('application/json', $headers['Content-Type']);
        //$this->assertSame(1,1);
    }
}


