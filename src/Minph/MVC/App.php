<?php


namespace Minph\MVC;

use Minph\MVC\Controller;
use Minph\Utility\Disposer;

use \Dotenv\Dotenv;
use \Tracy\Debugger;

/**
 * @class App
 *
 * Application static class utility.
 */
class App
{
    private static $instance;
    public $controller;
    private $errorHandler;

    /**
     * @method (static) instance
     * @return app instance
     */
    public static function instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new App();
        }
        return self::$instance;
    }

    /**
     * @method (static) boot
     * @param string `$appDir`
     * @return app instance
     */
    public static function boot(string $appDir)
    {
        define('MINPH_APP_HOME', $appDir);
        return self::instance();
    }


    /**
     * @method construct
     */
    public function __construct()
    {
        $env = Dotenv::createMutable(MINPH_APP_HOME);
        $env->load();
        $this->controller = new Controller();
        bindtextdomain('messages', MINPH_APP_HOME .'/locale');
        textdomain('messages');
    }

    public function __destruct()
    {
        $this->dispose();
    }

    /**
     * @method loadEnv
     * @param string `$dir`
     * @param string `$file`
     * @return app instance
     */
    public function loadEnv(string $dir, string $file)
    {
        $env = Dotenv::createMutable($dir, $file);
        $env->load();
        return $this;
    }

    /**
     * @method errorHandler
     * @param `$handler` error callback
     * @return app instance
     */
    public function errorHandler(callable $handler)
    {
        $this->errorHandler = $handler;
        return $this;
    }

    /**
     * @method (static) make
     * @param string `$classPath` class path
     * @return class object 
     *
     * It makes instance of a class.
     */
    public function make(string $classPath)
    {
        $path = MINPH_APP_HOME .$classPath .'.php';
        $classInfo = explode("\x2F", $path);
        $className = end($classInfo);
        $className = str_replace('.php', '', $className);
        require_once $path;
        return new $className;
    }

    /**
     * @method env
     * @param string `$key`
     * @param string `$default` (default = '')
     * @return string value in `.env`.
     *
     * It gets environment variables.
     */
    public function env(string $key, string $default = '')
    {
        $value = getenv($key);
        if (!isset($value)) {
            return $default;
        }
        return $value;
    }

    /**
     * @method subscribe
     * @return app instance
     *
     * It executes an specified controller method by `$appDirectory/routes.php`.
     */
    public function subscribe()
    {
        $uri = $this->getURI();
        try {
            $this->controller->route($uri);
        } catch (\Exception $e) {
            call_user_func_array($this->errorHandler, [$this, $e, $uri]);
        }
        return $this;
    }

    /**
     * @method dispose
     * @param Disposer `$disposer` (default = null)
     * @return app instance
     *
     * It disposes of an app instance.
     */
    public function dispose(Disposer $disposer = null)
    {
        if (isset($disposer)) {
            $disposer->dispose($this);
        }
        $instance = self::$instance;
        self::$instance = null;
        return $instance;
    }

    private function getURI()
    {
        $uri = '/';
        if (array_key_exists('REQUEST_URI', $_SERVER)) {
            $uri = $_SERVER['REQUEST_URI'];
        }
        return $uri;
    }
}

