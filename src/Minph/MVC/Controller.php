<?php


namespace Minph\MVC;

use Minph\Http\Header;
use Minph\Http\Input;

/**
 * @class Controller
 *
 * Controller object.
 */
class Controller
{
    private $routes;

    /**
     * @method construct
     */
    function __construct()
    {
        $routePath = MINPH_APP_HOME .'/routes.php';
        if (file_exists($routePath)) {
            $this->routes = include $routePath;
        }
    }

    /**
     * @method route
     * @param string `$uri` default = '/'
     * @param `$tag` default = null
     *
     * It executes an specified controller method by `MINPH_APP_HOME/routes.php`.
     */
    public function route(string $uri = '/', $tag = null)
    {
        $input = new Input();
        $parser = parse_url($uri);
        if (!is_array($parser)) {
            $this->redirect('/');
        }
        $path = $parser['path'];
        foreach ($this->routes as $key => $val) {
            $expr = '/^' .str_replace('/', '\/', $key) .'?.$/';
            preg_match($expr, $path, $matches);
            if (!empty($matches)) {
                $array = explode('@', $val);
                $classPath = $array[0];
                $method = $array[1];
                $classInfo = explode('/', $classPath);
                $className = end($classInfo);
                require_once MINPH_APP_HOME .'/controller/' .$classPath .'.php';
                $controller = new $className;
                $req = new \stdClass;
                $req->uri = $uri;
                $req->header = new Header();
                $req->input = $input->get();
                if (isset($req->input->get->locale)) {
                    $locale = $req->input->get->locale;
                    putenv('LC_ALL=' .$locale);
                    setlocale(LC_ALL, $locale);
                }
                $controller->{$method}($req, $tag);
                return;
            }
        }
        throw new \Exception("$path not found", 404);
    }
}
