<?php

namespace Minph\Utility;

/**
 * @class Minph\Utility\Pool
 */
class Pool
{
    private static $pool = [];

    private function __construct()
    {
    }

    /**
     * @method (static) clear
     */
    public static function clear()
    {
        self::$pool = [];
    }

    /**
     * @method (static) set
     * @param string `$alias`
     * @param object `$object`
     */
    public static function set(string $alias, $object)
    {
        self::$pool[$alias] = $object;
    }

    /**
     * @method (static) has 
     * @param string `$alias`
     * @return boolean If `Pool` has `$alias`, true. Otherwise, false.
     */
    public static function has(string $alias)
    {
        return isset(self::$pool[$alias]);
    }

    /**
     * @method (static) get
     * @param string `$alias`
     * @return object `$object`
     */
    public static function get(string $alias)
    {
        if (self::has($alias)) {
            return self::$pool[$alias];
        }
        return null;
    }

    /**
     * @method (static) remove
     * @param string `$alias`
     */
    public static function remove(string $alias)
    {
        if (isset(self::$pool[$alias])) {
            unset(self::$pool[$alias]);
        }
    }
}
