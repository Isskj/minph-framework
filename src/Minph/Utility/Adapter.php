<?php

namespace Minph\Utility;

use Minph\MVC\App;

/**
 * @interface Minph\Utility\Adapter
 *
 */
interface Adapter
{
    /**
     * @method setup
     * @param App `$app` app object
     *
     */
    public function setup(App $app);

    /**
     * @method dispose
     * @param App `$app` app object
     *
     */
    public function dispose(App $app);
}

