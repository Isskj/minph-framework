<?php

namespace Minph\Utility;

use Minph\MVC\App;

/**
 * @interface Minph\Utility\Disposer
 *
 */
interface Disposer
{
    /**
     * @method dispose
     * @param App `$app` app object
     *
     */
    public function dispose(App $app);
}

