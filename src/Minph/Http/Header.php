<?php

namespace Minph\Http;

use Minph\Exception\InputException;

/**
 * @function getallheaders
 *
 * This is used when getallheaders function doesn't exist. (Nginx, etc.)
 */
if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = [];
        if ($_SERVER) {
            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
        }
        return $headers;
    }
}


/**
 * @class Minph\Http\Header
 *
 * Header utility class.
 */
class Header
{

    /**
     * @method getMethod
     * @return string get http method
     */
    public function method()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            return strtolower($_SERVER['REQUEST_METHOD']);
        }
        return 'unknown';
    }

    /**
     * @method getHeaders
     * @return array header information
     */
    public function get()
    {
        return getallheaders();
    }

    /**
     * @method authBearer
     * @return header information of `Authorization: Bearer {TOKEN}`
     */
    public function authBearer()
    {
        $headers = getallheaders();
        if (isset($headers['Authorization'])) {
            $auth = $headers['Authorization'];
            if (preg_match('/^Bearer (.+)$/', $auth, $matches)) {
                if (isset($matches[1])) {
                    return $matches[1];
                }
            }
        }
        return null;
    }
}
