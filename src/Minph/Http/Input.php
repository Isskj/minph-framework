<?php

namespace Minph\Http;

use Minph\Exception\InputException;


/**
 * @class Minph\Http\Input
 *
 * User input utility class.
 */
class Input
{

    /**
     * @method get
     * @return array user input data
     */
    public function get()
    {
        $data = new \stdClass;
        $raw = file_get_contents('php://input');
        $data->get = empty($_GET) ? new \stdClass : json_decode(json_encode($_GET));
        $data->post = empty($_POST) ? new \stdClass : json_decode(json_encode($_POST));
        $data->raw = empty($raw) ? new \stdClass : $raw;
        return $data;
    }
}
