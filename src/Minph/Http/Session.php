<?php

namespace Minph\Http;

use Minph\App;
use Tracy\Debugger;

/**
 * @class Minph\Http\Session
 *
 * Session wrapper class.
 */
class Session
{
    private $sessionExpiration;

    /**
     * @method construct
     *
     */
    public function __construct()
    {
        $serverSessionExpiration = getenv('SERVER_SESSION_EXPIRATION');
        $sessionExpiration = getenv('SESSION_EXPIRATION');
        if ($serverSessionExpiration) {
            ini_set('session.gc_maxlifetime', $serverSessionExpiration);
        }
        if ($sessionExpiration) {
            $this->sessionExpiration = (int)$sessionExpiration;
        } else {
            $this->sessionExpiration = (int)60*5;
        }
        if (session_status() == PHP_SESSION_NONE) {
            session_set_cookie_params($this->sessionExpiration);
            session_start();
        }
    }

    /**
     * @method get
     * @param string `$key`
     * @return session value
     */
    public function get(string $key)
    {
        $now = time();
        $lastActivity = $this->getLastActivity();
        if ($lastActivity && ($now - $lastActivity) > $this->sessionExpiration) {
            $this->reset();
        }
        $_SESSION['last_activity'] = $now;
        if ($this->has($key)) {
            return $_SESSION[$key];
        }
        return null;
    }

    /**
     * @method has
     * @param string `$key`
     * @return boolean If session has the key, true. Otherwise, false.
     */
    public function has($key)
    {
        return isset($_SESSION[$key]);
    }

    /**
     * @method set
     * @param string `$key`
     * @param `$value`
     */
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @method remove
     * @param string `$key`
     */
    public function remove($key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * @method reset
     *
     * Reset the session.
     */
    public function reset()
    {
        session_unset();
        session_destroy();
        session_start();
    }

    private function getLastActivity()
    {
        if (isset($_SESSION['last_activity'])) {
            return (int)$_SESSION['last_activity'];
        }
        return false;
    }
}
