# Minph framework

Minph framework is the minimal framework based on MVC design.

## packagist
composer.json
```
~~~
"require": {
    "minph/minph-framework": "^1.0.0",
}
~~~
$ composer install
```
[https://packagist.org/packages/minph/minph-framework](https://packagist.org/packages/minph/minph-framework)


## How to install
```
$ vendor/bin/minph install .
$ make reinstall
$ vi app/.env
```

### Server requirements
* PHP >= 7.x
* PDO extension
* OpenSSL extension
* Mbstring extension
* Linux environment


## Project directory
* `app/`
  * `adapter/`  
    Adapter class. (DBAdapter.php, etc.)
  * `controller/`  
    Controller class. (TopController.php, etc.)
  * `locale/`  
    gettext localization files.
  * `migration/`  
    Database schema files. (tables.sql, etc.)
  * `service/`  
    Program logic class. (AuthService.php, etc.)
  * `storage/`  
    Template cache and log class. (Smarty template cache, etc.)
  * `test/`  
    Unit test class. (UserServiceTest.php, etc.)
  * `view/`  
    View template files. (index.tpl, etc.)
  * `public/`  
    Web document root directory.
  * `resource/`  
    Web resource directory.
  * `.env`
    Configuration file.
  * `boot.php`  
    It is an entry point of App.
  * `routes.php`
    It defines URI and Controller's mapping configuration.

# Classes

[Classes](./doc/Classes.md)
