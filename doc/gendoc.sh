#!/bin/bash

project_dir="`dirname $0`/.."
source "${project_dir}/vendor/bin/tc.common.sh"

tc.verbose_exec "echo -e \"# Classes\n\" > ${project_dir}/doc/Classes.md"
tc.verbose_exec "rm -rf ${project_dir}/doc/Minph"

for file in `find ${project_dir}/src -name "*.php"`
do
    namespace=`echo $file | sed 's/.*\/src\///g' | xargs dirname`
    filename=`basename $file`
    filename=${filename%.*}
    tc.verbose_exec "mkdir -p ${project_dir}/doc/${namespace}"
    tc.verbose_exec "tc.make_doc_class $file > ${project_dir}/doc/${namespace}/${filename}.md"
    tc.verbose_exec "echo -e \"* [${namespace}/${filename}](./${namespace}/${filename}.md)\n\" >> ${project_dir}/doc/Classes.md"
done
