# DB Settings

***This settings is for development server only.
Do not use this for production server.***
[https://mariadb.com/kb/en/documentation/](https://mariadb.com/kb/en/documentation/)

## MariaDB

### install mariadb
```
$ sudo apt install mariadb
$ sudo mysql_secure_installation
```

### create user
```
$ mysql -u root -p
MariaDB [(none)] > create user '{yourname}' identified by 'password';
MariaDB [(none)] > create user '{yourname}'@'localhost' identified by 'password';
MariaDB [(none)] > select user, host from mysql.user;
MariaDB [(none)] > grant all privileges on *.* to '{yourname}' identified by 'password';
MariaDB [(none)] > grant all privileges on *.* to '{yourname}'@'localhost' identified by 'password';
MariaDB [(none)] > flush privileges;
MariaDB [(none)] > show grants for '{yourname}';
```

### Enable remote access if needed. 
```
vi /etc/mysql/mariadb.conf.d/50-server.cnf
~~~ 
bind-address = 0.0.0.0
~~~
$ mysql -u {yourname} -p
MariaDB [(none)] > show grants for '{yourname}';
+---------------------------------------------------------------------------------------------------------------+
| Grants for {yourname}@%                                                                                       |
+---------------------------------------------------------------------------------------------------------------+
| GRANT ALL PRIVILEGES ON *.* TO '{yourname}'@'%' IDENTIFIED BY PASSWORD '.........'                            |
+---------------------------------------------------------------------------------------------------------------+
```
