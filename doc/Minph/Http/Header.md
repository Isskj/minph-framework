
## @function getallheaders
 
This is used when getallheaders function doesn't exist. (Nginx, etc.)
 
# @class Minph\Http\Header
 
Header utility class.
 
## @method getMethod
* @return string get http method
 
## @method getHeaders
* @return array header information
 
## @method authBearer
* @return header information of `Authorization: Bearer {TOKEN}`
 
> Generated by [tc.make_doc_class](https://bitbucket.org/isskj/toolc)
