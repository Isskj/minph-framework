<?php

require_once __DIR__ .'/../vendor/autoload.php';

use Minph\MVC\App;
use Tracy\Debugger;


function errorHandler($app, $e, $path) {
    $code = $e->getCode();
    if ($code < 400) {
        $code = 500;
    }
    $view = $app->make('/module/ViewComponent');
    if (preg_match('/\/api\//', $path, $matches)) {
        $errorModel = [
            'message' => $e->getMessage()
        ];
        http_response_code($code);
        $view->json($errorModel);
    } else {
        $app->controller->route('/error', $e);
    }
}

$app = App::boot(__DIR__);

$disposer = $app->make('/module/AppDisposer');
$debugger = $app->make('/module/DebugComponent');

$app->errorHandler('errorHandler')
    ->subscribe()
    ->dispose($disposer);
