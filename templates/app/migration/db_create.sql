drop database if exists minph;
create database minph;
use minph;

create table if not exists user
(
    uid bigint not null auto_increment,
    email varchar(512) not null,
    password varchar(512) not null,
    name varchar(128) default "",
    age int default 0,
    token varchar(512) default "",
    refreshToken varchar(512) default "",
    createat varchar(50) default "",
    updateat varchar(50) default "",
    primary key (uid)
);
