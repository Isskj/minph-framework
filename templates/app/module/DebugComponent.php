<?php

use Minph\MVC\App;

use Tracy\Debugger;

class DebugComponent
{
    public function __construct()
    {
        $app = App::instance();
        $dir = MINPH_APP_HOME .'/storage/log';
        if ($app->env('DEBUG', 'false') === 'true') {
            Debugger::enable(Debugger::DEVELOPMENT, $dir);
        } else {
            Debugger::enable(Debugger::PRODUCTION, $dir);
        }
        $uri = '/';
        if (array_key_exists('REQUEST_URI', $_SERVER)) {
            $uri = $_SERVER['REQUEST_URI'];
        }
        if (preg_match('/\/api\//', $uri, $matches)) {
            Debugger::$showBar = false;
        }
    }
}
