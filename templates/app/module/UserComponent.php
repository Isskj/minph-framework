<?php

use Minph\MVC\App;
use Minph\Repository\DB;
use Minph\Crypto\EncoderAES256;
use Minph\Http\Session;

class UserComponent
{
    private $db;

    public function __construct()
    {
        $app = App::instance();
        $db = $app->make('/module/DBComponent');
        $this->db = $db->db();
    }

    public function info($token)
    {
        // validation
        $this->validateNull($token, 'validation.error.token.null');
        $foundUser = $this->db->queryOne('select email, name, age from user where token = :token',
            [ 'token' => $token ]);
        if (!isset($foundUser)) {
            throw new \Exception(_('error.user.not_found'), 404);
        }
        $foundUser = json_decode(json_encode($foundUser));
        return $foundUser;
    }

    private function validateNull($obj, $msg)
    {
        if (!isset($obj)) {
            throw new \Exception(_($msg));
        }
    }
}
