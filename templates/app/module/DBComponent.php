<?php

use Minph\MVC\App;
use Minph\Repository\DB;
use Minph\Utility\Pool;

class DBComponent
{
    private $db;

    public function __construct()
    {
        $app = App::instance();
        if (!Pool::has('db_default')) {
            $db = new DB(
                $app->env('DATABASE_DSN'),
                $app->env('DATABASE_USERNAME'),
                $app->env('DATABASE_PASSWORD'));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
            $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            Pool::set('db_default', $db);
        }
        $this->db = Pool::get('db_default');
    }

    public function db()
    {
        return $this->db;
    }
}

