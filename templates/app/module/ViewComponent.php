<?php

use Minph\MVC\App;
use Minph\Repository\DB;
use Minph\Utility\Pool;

class ViewComponent
{
    private $engine;

    public function __construct()
    {
        $app = App::instance();
        if (!Pool::has('db_default')) {
            $engine = new Smarty();
            $engine->setTemplateDir(MINPH_APP_HOME . '/view');
            $engine->setCompileDir(MINPH_APP_HOME . '/storage/template/smarty/templates_c');
            $engine->setCacheDir(MINPH_APP_HOME . '/storage/template/smarty/cache');
            $engine->error_reporting = 0;
            if ($app->env('DEBUG') === 'true') {
                $engine->debugging = true;
            }
            Pool::set('view_default', $engine);
        }
        $this->engine = Pool::get('view_default');
    }

    public function html($file, $model = null)
    {
        if (!empty($model)) {
            $this->engine->assign($model);
        }
        $this->engine->display($file);
    }

    public function json($model = null)
    {
        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($model);
    }
}

