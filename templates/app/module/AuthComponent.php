<?php

use Minph\MVC\App;
use Minph\Repository\DB;
use Minph\Crypto\EncoderAES256;
use Minph\Http\Session;

class AuthComponent
{
    const TOKEN_SEED_LEN = 32;
    private $encoder;
    private $db;

    public function __construct()
    {
        $app = App::instance();
        $this->encoder = new EncoderAES256($app->env('AES256_CBC_KEY'));
        $db = $app->make('/module/DBComponent');
        $this->db = $db->db();
    }

    public function createAccount($user)
    {
        // validation
        $this->validateNull($user, 'validation.error.user.null');
        $this->validateNull($user->email, 'validation.error.user.email.null');
        $this->validateNull($user->password, 'validation.error.user.password.null');
        $this->validateEmail($user->email, 'validation.error.user.email.invalid');

        // search user
        $foundUser = $this->db->queryOne('select uid from user where email = :email',
            [ 'email' => $user->email ]);
        if (isset($foundUser)) {
            throw new \Exception(_('error.createAccount.user_already_registered'), 406);
        }

        // create user model
        $newUser = new stdClass;
        $newUser->name = $user->name;
        $newUser->email = $user->email;
        $newUser->age = (int)$user->age;
        $newUser->password = $this->encoder->encrypt($user->password);
        $newUser->token = $this->createToken();
        $newUser->refreshToken = $this->createToken();
        $newUser->createat = date('c');
        $newUser->updateat = date('c');

        $ret = $this->db->insert('user', (array)$newUser);
        if (!isset($ret) || $ret !== 1) {
            throw new \Exception(_('error.user.create_account_failed'), 406);
        }
        unset($newUser->password);
        unset($newUser->createat);
        unset($newUser->updateat);
        return $newUser;
    }

    public function deleteAccount($user)
    {
        $foundUser = $this->login($user);
        $this->db->delete('user', 'uid', $foundUser->uid);
    }

    public function validateToken($token)
    {
        $this->validateNull($token, 'validation.error.user.token.null');
        $foundUser = $this->db->queryOne('select uid, token from user where token = :token',
            [ 'token' => $token ]);
        if (!isset($foundUser)) {
            throw new \Exception(_('error.user.token_not_found'), 404);
        }
        $foundUser = json_decode(json_encode($foundUser));
        return $foundUser;
    }

    public function refreshToken($refreshToken)
    {
        $this->validateNull($refreshToken, 'validation.error.user.token.null');

        $foundUser = $this->db->queryOne('select uid, token, refreshToken from user where refreshToken = :refreshToken',
            [ 'refreshToken' => $refreshToken ]);
        if (!isset($foundUser)) {
            throw new \Exception(_('error.user.token_not_found'), 404);
        }
        $foundUser = json_decode(json_encode($foundUser));

        // update token
        $updateat = date('c');
        $newToken = $this->createToken();
        $newRefreshToken = $this->createToken();
        $affected = $this->db->execute('UPDATE user SET token = :token, refreshToken = :refreshToken, updateat = :updateat WHERE uid = :uid',
            [ 'token' => $newToken, 'refreshToken' => $newRefreshToken, 'updateat' => $updateat, 'uid' => $foundUser->uid ]);
        if (!isset($affected) || $affected === 0) {
            throw new \Exception(_('error.user.refreshToken_failed'), 406);
        }
        $foundUser->token = $newToken;
        $foundUser->refreshToken = $newRefreshToken;
        return $foundUser;
    }

    public function login($user)
    {
        // validation
        $this->validateNull($user, 'validation.error.user.null');
        $this->validateNull($user->email, 'validation.error.user.email.null');
        $this->validateNull($user->password, 'validation.error.user.password.null');

        $foundUser = $this->db->queryOne('select uid, email, password, token, refreshToken from user where email = :email',
            [ 'email' => $user->email ]);
        if (!isset($foundUser)) {
            throw new \Exception(_('error.user.not_found'), 404);
        }
        $foundUser = json_decode(json_encode($foundUser));

        // check password
        $password = $this->encoder->decrypt($foundUser->password);
        if ($password !== $user->password) {
            throw new \Exception(_('error.user.password_validation_failed'), 401);
        }

        // update token
        $updateat = date('c');
        $newToken = $this->createToken();
        $newRefreshToken = $this->createToken();
        $affected = $this->db->execute('UPDATE user SET token = :token, refreshToken = :refreshToken, updateat = :updateat WHERE uid = :uid',
            [ 'token' => $newToken, 'refreshToken' => $newRefreshToken, 'updateat' => $updateat, 'uid' => $foundUser->uid ]);
        if (!isset($affected) || $affected === 0) {
            throw new \Exception(_('error.user.refreshToken_failed'), 406);
        }
        $foundUser->token = $newToken;
        $foundUser->refreshToken = $newRefreshToken;

        unset($foundUser->password);
        return $foundUser;
    }

    public function logout($token)
    {
        $foundUser = $this->validateToken($token);
        $updateat = date('c');
        $affected = $this->db->execute('UPDATE user SET token = :token, refreshToken = :refreshToken, updateat = :updateat WHERE uid = :uid',
            [ 'token' => '', 'refreshToken' => '', 'updateat' => $updateat, 'uid' => $foundUser->uid ]);
        if (!isset($affected) || $affected === 0) {
            throw new \Exception(_('error.user.refreshToken_failed'), 406);
        }
    }

    private function validateNull($obj, $msg)
    {
        if (!isset($obj)) {
            throw new \Exception(_($msg));
        }
    }

    private function validateEmail($email, $msg)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new \Exception(_('validation.error.user.email.invalid'));
        }
    }

    private function createToken()
    {
        return hash('sha256', bin2hex(random_bytes(self::TOKEN_SEED_LEN)));
    }
}
