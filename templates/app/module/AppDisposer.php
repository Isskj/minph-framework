<?php

use Minph\MVC\App;
use Minph\Utility\Disposer;
use Minph\Utility\Pool;


class AppDisposer implements Disposer
{
    public function dispose(App $app)
    {
        Pool::clear();
    }
}

