<?php

return [
    '/' => 'web/TopController@index',
    '/auth' => 'web/AuthWebController@index',
    '/auth/signup' => 'web/AuthWebController@signup',
    '/auth/login' => 'web/AuthWebController@login',
    '/user' => 'web/UserWebController@index',
    '/error' => 'web/WebErrorController@index',
    '/api/auth/signup' => 'api/AuthAPIController@signup',
    '/api/auth/login' => 'api/AuthAPIController@login',
    '/api/auth/logout' => 'api/AuthAPIController@logout',
    '/api/auth/resign' => 'api/AuthAPIController@resign',
    '/api/auth/token' => 'api/AuthAPIController@token',
    '/api/user' => 'api/UserAPIController@userInfo'
];

