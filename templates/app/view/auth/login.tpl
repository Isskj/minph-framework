{include file='parts/header.tpl'}

<div class="card-deck mx-auto" style="width:500px;">
    <div class="card mb-4 box-shadow">
        <h4 class="card-header text-center bg-dark text-white">Login</h4>
        <form id="form_login" class="card-body d-flex flex-column">
            <div class="form-group">
                <label for="input_email">Email address</label>
                <input type="email" class="form-control" id="input_email">
            </div>
            <div class="form-group">
                <label for="input_password">Password</label>
                <input type="password" class="form-control" id="input_password">
            </div>
            <button type="submit" class="btn btn-lg btn-block btn-outline-dark mt-auto">
                Login
                {include file='parts/spinner.tpl'}
            </button>
        </form>
    </div>
</div>
<div class="mx-auto" style="width:500px;">
    <a class="ml-3 btn btn-outline-dark p-3" href="/auth/signup">>> Signup</a>
</div>

{include file='parts/footer.tpl'}
