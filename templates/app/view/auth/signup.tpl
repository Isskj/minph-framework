{include file='parts/header.tpl'}

<div class="card-deck mx-auto" style="width:500px;">
    <div class="card mb-4 box-shadow">
        <h4 class="card-header text-center bg-dark text-white">Signup</h4>
        <form id="form_signup" class="card-body d-flex flex-column">
            <div class="form-group">
                <label for="input_email">Email address</label>
                <input type="email" class="form-control" id="input_email">
            </div>
            <div class="form-group">
                <label for="input_password">Password</label>
                <input type="password" class="form-control" id="input_password">
            </div>
            <div class="form-group">
                <label for="input_name">Name</label>
                <input type="text" class="form-control" id="input_name">
            </div>
            <div class="form-group">
                <label for="input_age">Age</label>
                <input type="number" class="form-control" id="input_age">
            </div>
            <button type="submit" class="btn btn-lg btn-block btn-outline-dark mt-auto">
                Signup
                {include file='parts/spinner.tpl'}
            </button>
        </form>
    </div>
</div>

<div class="mx-auto" style="width:500px;">
    <a class="ml-3 btn btn-outline-dark p-3" href="/auth/login">>> Login</a>
</div>

{include file='parts/footer.tpl'}
