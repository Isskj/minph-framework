{include file='parts/header.tpl'}

<div class="col-md-4">
    <div class="alert alert-warning">
        {$error.status} {$error.message}
        <a class="alert-link" href="/">Go to Top</a>
    </div>
</div>

{include file='parts/footer.tpl'}
