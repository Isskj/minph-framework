{include file='parts/header.tpl'}

<div class="card-deck mx-auto" style="width:500px;">
    <div class="card box-shadow">
        <h4 class="card-header text-center bg-dark text-white">User</h4>
        <div class="card-text table-responsive">
            <table id="user_table" class="table table-hover">
                <tr>
                    <td>Email:</td>
                    <td id="user_email"></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td id="user_name"></td>
                </tr>
                <tr>
                    <td>Age:</td>
                    <td id="user_age"></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="mx-auto" style="width:500px;">
    <button id="button_logout" class="m-3 btn btn-outline-dark p-3">
        Logout {include file='parts/spinner.tpl'}
    </button>
</div>

{include file='parts/footer.tpl'}
