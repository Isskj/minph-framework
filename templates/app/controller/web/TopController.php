<?php

use Minph\MVC\App;
use Minph\Http\Session;

class TopController
{
    private $view;
    private $session;

    public function __construct()
    {
        $this->view = App::instance()->make('/module/ViewComponent');
        $this->session = new Session();
    }

    public function index($request, $tag)
    {
        $model = [
            'user' => $this->session->get('user')
        ];
        $this->view->html('top.tpl', $model);
    }
}
