<?php

use Minph\MVC\App;

class WebErrorController
{
    private $view;

    public function __construct()
    {
        $this->view = App::instance()->make('/module/ViewComponent');
    }

    public function index($request, $tag)
    {
        $model = [
            'error' => [
                'status' => 500,
                'message' => 'Internal Server Error',
            ]
        ];
        if ($tag instanceof \Exception) {
            $model['error']['status'] = $tag->getCode();
            $model['error']['message'] = $tag->getMessage();
        }
        $this->view->html('error/index.tpl', $model);
    }
}
