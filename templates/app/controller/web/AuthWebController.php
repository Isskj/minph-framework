<?php

use Minph\MVC\App;
use Minph\Http\Session;

class AuthWebController
{
    private $view;

    public function __construct()
    {
        $this->view = App::instance()->make('/module/ViewComponent');
    }

    public function index($request, $tag)
    {
        $this->view->html('auth/signup.tpl');
    }

    public function signup($request, $tag)
    {
        $this->view->html('auth/signup.tpl');
    }

    public function login($request, $tag)
    {
        $this->view->html('auth/login.tpl');
    }
}
