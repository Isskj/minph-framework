<?php

use Minph\MVC\App;
use Minph\Http\Session;

class UserWebController
{
    private $view;

    public function __construct()
    {
        $this->view = App::instance()->make('/module/ViewComponent');
    }

    public function index($request, $tag)
    {
        $this->view->html('user/index.tpl');
    }
}
