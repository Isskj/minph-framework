<?php

use Minph\MVC\App;
use Tracy\Debugger;

class UserAPIController
{
    private $user;
    private $view;

    public function __construct()
    {
        $this->user = App::instance()->make('/module/UserComponent');
        $this->view = App::instance()->make('/module/ViewComponent');
    }

    public function userInfo($request, $tag)
    {
        $token = $request->header->authBearer();
        $this->view->json($this->user->info($token));
    }
}

