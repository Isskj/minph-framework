<?php

use Minph\MVC\App;
use Tracy\Debugger;

class AuthAPIController
{
    private $auth;
    private $view;

    public function __construct()
    {
        $this->auth = App::instance()->make('/module/AuthComponent');
        $this->view = App::instance()->make('/module/ViewComponent');
    }

    public function signup($request, $tag)
    {
        if ($request->header->method() !== 'post') {
            throw new \Exception($request->header->method() .' not accepted', 405);
        }
        $user = json_decode($request->input->raw);
        $ret = $this->auth->createAccount($user);
        $this->view->json($ret);
    }

    public function login($request, $tag)
    {
        if ($request->header->method() !== 'post') {
            throw new \Exception($request->header->method() .' not accepted', 405);
        }
        $user = json_decode($request->input->raw);
        $ret = $this->auth->login($user);
        $this->view->json($ret);
    }

    public function logout($request, $tag)
    {
        if ($request->header->method() !== 'post') {
            throw new \Exception($request->header->method() .' not accepted', 405);
        }
        $token = $request->header->authBearer();
        $this->auth->logout($token);
        $model = [
            'message' => 'user logged out'
        ];
        $this->view->json($model);
    }

    public function resign($request, $tag)
    {
        if ($request->header->method() !== 'post') {
            throw new \Exception($request->header->method() .' not accepted', 405);
        }
        $user = json_decode($request->input->raw);
        $this->auth->deleteAccount($user);
    }

    public function token($request, $tag)
    {
        $token = $request->header->authBearer();
        switch ($request->header->method()) {
        case 'post':
            $user = json_decode($request->input->raw);
            $ret = $this->auth->refreshToken($user->refreshToken);
            break;
        case 'get':
            $ret = $this->auth->validateToken($token);
            break;
        default:
            throw new \Exception($request->header->method() .' not accepted', 405);
        }
        $this->view->json($ret);
    }
}

