window.app.documentReadyCB.push(function() {
    const form = $('#form_login');
    if (form.length === 0) {
        return;
    }

    rxjs.fromEvent(form, 'submit')
        .pipe(
            rxjs.operators.concatMap(function(e) {
                e.preventDefault();
                return window.app.formAuthRequest(form, '/api/auth/login');
            }),
        ).subscribe(function(result) {
            window.app.formAuthResponse(form, result, null);
        }, function(error) {
            window.app.formAuthResponse(form, null, error);
        });
});
