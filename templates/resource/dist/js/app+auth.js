window.app.formAuthRequest = function(form, api) {
    // show loading
    $(form).find(':submit').attr('disabled', 'disabled');
    $(form).find('#loading_spinner').show();
    // create user model
    var user = {};
    $(form).find(':input').each(function(idx, item) {
        if (item.id && item.id.indexOf('input_') == 0) {
            var id = item.id.replace('input_', '');
            user[id] = item.value;
        }
    });
    if (user.age) {
        var age = parseInt(user.age);
        user.age = isNaN(age) ? 0 : age;
    }
    // call ajax 
    return rxjs.ajax.ajax({
        url: api,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(user)
    });
};

window.app.formAuthResponse = function(form, result, error) {
    // hide loading
    $(form).find(':submit').removeAttr('disabled');
    $(form).find('#loading_spinner').hide();
    if (error) {
        console.error(error);
        if (error.response) {
            window.app.showErrorMessage(error.response);
        } else {
            window.app.showErrorMessage(error);
        }
    } else {
        window.app.showInfoMessage('Success',
            JSON.stringify(result.response), function(e) {
                window.app.setToken(result.response);
                window.location.href = '/user';
            });
    }
};
