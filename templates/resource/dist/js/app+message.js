window.app.documentReadyCB.push(function() {
    window.app.resetMessage();
});

window.app.showMessage = function(title, message, stat = 'info', callback) {
    window.app.resetMessage();
    switch (stat) {
        case 'info':
            $('#message_modal').find('.modal-title').addClass('text-info');
            break;
        case 'warning':
            $('#message_modal').find('.modal-title').addClass('text-warning');
            break;
        case 'error':
            $('#message_modal').find('.modal-title').addClass('text-danger');
            break;
    }
    $('#message_modal').find('.modal-title').text(title);
    $('#message_modal').find('.modal-body').html('<p>' + message + '</p>');
    $('#message_modal').modal('show');
    $('#message_modal').on('hidden.bs.modal', function(e) {
        if (callback) {
            callback(e);
        }
    });
};

window.app.showInfoMessage = function(title, message, callback = null) {
    window.app.showMessage(title, message, 'info', callback);
};

window.app.showWarningMessage = function(title, message, callback = null) {
    window.app.showMessage(title, message, 'warning', callback);
};

window.app.showErrorMessage = function(error, callback = null) {
    if (error.message && error.message !== '') {
        window.app.showMessage('Error', error.message, 'error', callback);
    } else {
        window.app.showMessage('Error', 'System Error', 'error', callback);
    }
};

window.app.resetMessage = function() {
    $('#message_modal').find('.modal-title').removeClass('text-info');
    $('#message_modal').find('.modal-title').removeClass('text-warning');
    $('#message_modal').find('.modal-title').removeClass('text-danger');
    $('#message_modal').modal('hide');
};
