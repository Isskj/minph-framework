class App
{
    constructor()
    {
        this.documentReadyCB = [];
    }

    documentReady()
    {
        var cb;
        while ((cb = this.documentReadyCB.shift())) {
            cb();
        }
    }
}

window.app = new App();


$(document).ready(function(){
    window.app.documentReady();
});
