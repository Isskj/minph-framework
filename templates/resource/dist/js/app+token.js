window.app.documentReadyCB.push(function() {
    window.app.updateToken();
});

window.app.setToken = function(response) {
    var epochTS = Math.round((new Date()).getTime()/1000) + 5 * 60;
    window.localStorage.setItem('token', response.token);
    window.localStorage.setItem('tokenExpiration', epochTS);
    window.localStorage.setItem('refreshToken', response.refreshToken);
    console.log('token', response.token);
    console.log('tokenExpiration', epochTS);
    console.log('refreshToken', response.refreshToken);
};

window.app.updateToken = function() {
    var user = {
        token: window.localStorage.getItem('token'),
        refreshToken: window.localStorage.getItem('refreshToken'),
        exTime: window.localStorage.getItem('tokenExpiration'),
    };
    console.log('token', user);
    if (!user.token || !user.refreshToken || !user.exTime) {
        return;
    }

    var epochTS = Math.round((new Date()).getTime()/1000);
    if (user.exTime && (user.exTime - epochTS) > 0) {
        // no token update
        return;
    }

    rxjs.ajax.ajax({
        url: '/api/auth/token',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(user)
    }).subscribe(function(result) {
        window.app.setToken(result.response);
    }, function(error) {
        window.app.clearToken();
    });
};

window.app.clearToken = function() {
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('refreshToken');
    window.localStorage.removeItem('tokenExpiration');
};
