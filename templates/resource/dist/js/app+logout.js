window.app.documentReadyCB.push(function() {
    const button = $('#button_logout');
    console.log(button);
    if (button.length === 0) {
        return;
    }

    rxjs.fromEvent(button, 'click')
        .pipe(
            rxjs.operators.concatMap(function(e) {
                var token = window.localStorage.getItem('token');
                $(button).attr('disabled', 'disabled');
                $(button).find('#loading_spinner').show();
                // call ajax 
                return rxjs.ajax.ajax({
                    url: '/api/auth/logout',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8',
                        'Authorization': 'Bearer ' + token,
                    }
                });
            }),
        ).subscribe(function(result) {
            window.app.clearToken();
            window.location.href = '/';
        }, function(error) {
            $(button).removeAttr('disabled');
            $(button).find('#loading_spinner').hide();
        });
});
