window.app.documentReadyCB.push(function() {
    const $table = $('#user_table');
    if ($table.length === 0) {
        return;
    }

    // call ajax 
    var token = window.localStorage.getItem('token');
    return rxjs.ajax.ajax({
        url: '/api/user',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer ' + token
        },
    }).subscribe(function(result) {
        $table.find('#user_email').text(result.response.email);
        $table.find('#user_name').text(result.response.name);
        $table.find('#user_age').text(result.response.age);
    }, function(error) {
        console.error(error);
        //window.location.href = '/';
    });
});
