#!/bin/bash

distdir="../dist"

source "`dirname $0`/../../vendor/bin/tc.common.sh" 2>/dev/null
source "`dirname $0`/../../bin/tc.common.sh" 2>/dev/null

tc.verbose_exec "find ${distdir}/js -name '*.js' | xargs cat > app_tmp1.js"
tc.verbose_exec "tc.trim_comment app_tmp1.js > app_tmp2.js"
tc.verbose_exec "cat app_tmp2.js | tr -d '\n' | sed 's/  //g' > ${distdir}/app_pack.js"
tc.verbose_exec "rm -f *.js"

